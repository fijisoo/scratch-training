module.exports = {
  webpackConfig: Object.assign({}, require('./webpack.config.js'), {}),
  components: 'src/components/**/[A-Z]*.tsx',
  propsParser: require('react-docgen-typescript').withDefaultConfig('./tsconfig.json').parse
};
