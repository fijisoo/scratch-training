#### React app made form scratch

##### Configured 

###### webpack 4.27
    -
    1. webpack-cli
    2. webpack-dev-server
    3. html-webpack-plugin
    
###### CSS-Modules and SCSS
    -
    1. node-sass
    
    2. style-loader
    3. css-loader
        - modules: true
    4. sass-loader
    
###### React 
    -
    1. react-dom
    2. babel-loader
    
###### Typescript
    -
    1. typescript
    2. ts-loader
    
###### DevDependencies
    1. prettier
    2. tslint
    3. styleguidist
