type TRounded = 10 | 20 | 30 | 40 | 50;

export interface ImageProps {
  /**
   * Source to image
   */
  src: string;
  /**
   * alt name for image
   */
  alt?: string;
  /**
   * image title
   */
  title: string;
  /**
   * if corners should be rounded set rounded value
   */
  rounded?: TRounded;
  /**
   * custom classNames goes here
   */
  className?: string;
  /**
   * if width image is fitted to width if height -> height
   */
  responsive?: 'width' | 'height';
  /**
   * set true if size of image should be fitted to wrapper
   */
  fitted?: boolean;
  /**
   * onclick function
   */
  onClick?: () => void;
}
