<style>
.imageWrapper {
    width: 200px;
    height: 200px;
    border: 1px solid black;
    margin: 5px;
}
.imageWrapperFlex {
    display: flex;
    width: 50%;
    height: 200px;
    border: 1px solid black;
    margin: 5px;
}
</style>

static wrapper size / responsive height

```jsx
    const image = require('../../assets/images/main.jpg');
    <div className="imageWrapper">
      <Image src={image} alt={'image'} title={'imageTitle'} fitted={false} responsive={'height'} />
    </div>
```
static wrapper size / responsive width

```jsx
    const image = require('../../assets/images/main.jpg');
    <div className="imageWrapper">
      <Image src={image} alt={'image'} title={'imageTitle'} fitted={false} responsive={'width'} />
    </div>
```

static wrapper size / fitted true
```jsx
    const image = require('../../assets/images/main.jpg');
    <div className="imageWrapper">
      <Image src={image} alt={'image'} title={'imageTitle'} fitted={true} />
    </div>
```
flex wrapper size / esponsive height
```jsx
    const image = require('../../assets/images/main.jpg');
    <div className="imageWrapperFlex">
      <Image src={image} alt={'image'} title={'imageTitle'} fitted={false} responsive={'height'} />
    </div>
```
flex wrapper size / esponsive width
```jsx
    const image = require('../../assets/images/main.jpg');
    <div className="imageWrapperFlex">
      <Image src={image} alt={'image'} title={'imageTitle'} fitted={false} responsive={'width'} />
    </div>
```
flex wrapper size / fitted true
```jsx
    const image = require('../../assets/images/main.jpg');
    <div className="imageWrapperFlex">
      <Image src={image} alt={'image'} title={'imageTitle'} fitted={true}/>
    </div>
```
static wrapper size / fitted true / rounded 50
```jsx
    const image = require('../../assets/images/main.jpg');
    <div className="imageWrapper">
        <Image src={image} alt={'image'} title={'imageTitle'} fitted={true} rounded={50}/>
    </div>
```
