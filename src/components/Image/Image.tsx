import React from 'react';
import { ImageProps } from './Image.types';

import c from './image.scss';

export function Image(props: ImageProps) {
  const { src, alt, title, rounded, className, responsive, fitted, onClick } = props;

  const imageStyles = () => {
    const styles = [c.image];
    if (responsive) {
      styles.push(c[`image-responsive-${responsive}`]);
    }
    if (rounded) {
      styles.push(c[`image-rounded-${rounded}`]);
    }
    if (fitted) {
      styles.push(c[`image-fitted`]);
    }
    if (className) {
      styles.push(className);
    }
    return styles.join(' ');
  };
  return <img alt={alt} src={src} title={title} className={imageStyles()} onClick={onClick} />;
}
