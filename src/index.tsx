import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './App';

import c from './styles/index.scss';

const domContainer = document.querySelector('.myApp');
if (domContainer) {
  domContainer.classList.add(c.myApp);
}

ReactDOM.render(<App />, domContainer);
