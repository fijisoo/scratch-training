import React from 'react';
import { Image } from './components/Image';

import image from './assets/images/main.jpg';

import c from './app.scss';

export const App = () => {
  return (
    <div className={c.main}>
      <div className={c.mainWrapper}>
        <div className={c.main__image}>
          <Image src={image} alt={'image'} title={'imageTitle'} fitted={true} responsive={'height'} />
        </div>
        <div className={c.main__image}>
          <Image src={image} alt={'image'} title={'imageTitle'} fitted={true} responsive={'height'} />
        </div>
        <div className={c.main__image}>
          <Image src={image} alt={'image'} title={'imageTitle'} fitted={true} responsive={'height'} />
        </div>
      </div>
    </div>
  );
};
